
# coding: utf-8

# In[1]:

'''
    Mustafa Khan ~ a10776158
    Armon Safai- A13215141
    Zeyangyi Wang - A53106877
'''

import numpy as np
import operator
from copy import copy


# In[8]:

# Load the training dataset
X_train = []
y_train = []

with open('hw4train.txt') as train_data:
    for l in train_data:
        vector = [float(i) for i in l.split()]
        X_train.append(vector[:819])
        y_train.append(vector[-1])
        
X_test = []
y_test = []

# load the test dataset
with open('hw4test.txt') as test_data:
    for l in test_data:
        vector = [float(i) for i in l.split()]
        X_test.append(vector[:819])
        y_test.append(vector[-1])


# In[3]:

# filter the training and testing instance to those with labels of 1 and 2
# associate +1 with label 1 and -1 with label 2
train_12 = [(x, 1) if y == 1 else (x,-1)  for x,y in zip(X_train, y_train) if y == 1 or y == 2]
test_12 = [(x, 1) if y == 1 else (x,-1)  for x,y in zip(X_test, y_test) if y == 1 or y == 2]


# In[15]:

# perceptron implmentation as a function

# @param (array) data ~ Collection of x,y instances
# @return (array) w ~ weight vector
def perceptron(data, w=None):
    # weight vector intialized to all zeros if no w is passed in 
    if w == None:
        w = np.array([0 for i in xrange(819)])

    for x,y in data:
        # <w,x>
        sign = np.dot(np.array(x), w.T)

        # y*<w,x>
        if sign*y <= 0:
            # it predicted incorrectly, update weight vector
            w = w + np.dot(x,y)
        else:
            # it predicted correctly
            continue
    return w


# In[25]:

# perceptron implementaiton 

# intialize weight vector to 0s
weight_vector = np.array([0 for i in xrange(819)])

# record the weight vector after each pass through the
# training data
weight_vector_record = {1: None, 2: None, 3: None, 4: None}

# conduct training on the datset by
# iterating through each training instance for 3 passes
# after each pass we record the weight vector that amount of passes
weight_vector = np.array([0 for i in xrange(819)])

for pass_key in weight_vector_record:    
    weight_vector = perceptron(train_12, weight_vector)
    
    weight_vector_record[pass_key] = copy(weight_vector)


# In[26]:

# measure the accuracy of trained model on the training data itself
# through weight vector for each number of passes through the training data
for pass_key in [1,2,3,4]:
    # check out how many of the trainign isntances are labeled correctly
    correct_count = 0
    # use the weight vector from the nth pass over the training data
    weight_vector = weight_vector_record[pass_key]
    
    for x,y in train_12:
        sign = np.dot(np.array(x), weight_vector.T)

        # it's wrong
        if sign*y <= 0:
            continue
        # it's correct
        else:
            correct_count += 1
        
    train_error = (len(train_12) - correct_count)/float(len(train_12))
    print("Training error on basic perceptron is %f after %i passes through the training data" % (train_error, pass_key))

# create a space between the print statemetns
print "\n"
    
# measure the accuracy of trained model on the testing data
# through the weight vector for each number of passes through the training data
for pass_key in [1,2,3,4]:
    # check out how  many of the testing instance are predicted correctly
    correct_count = 0
    weight_vector = weight_vector_record[pass_key]
    for x,y in test_12:
        sign = np.dot(np.array(x), weight_vector.T)

        # it's wrong
        if sign*y <= 0:
            continue
        # it's correct
        else:
            correct_count += 1
        
    test_error = (len(test_12) - correct_count)/float(len(test_12))
    print("Testing error on basic perceptron is %f after %i passes through the training data" % (test_error, pass_key))


# In[7]:

# voted perceptron and average perceptron implementation 

# intialize weight vector to 0s
weight_vector = np.array([0 for i in xrange(819)])
# c ~ record how long a weight vector goes with out chaning
c = {}
# w ~ how many times the weight vector changes
w = 0
# record the weight vector and how long they persist after each weight change
passes = {}

for pass_key in [1,2,3,4]:
    # iterate through each of the training instances
    for x,y in train_12:
        # calculate the sign it generates
        # <w,x>
        sign = np.dot(np.array(x), weight_vector.T)

        # check if it's wrong and if the weight vector needs to be updated
        # y*<w,x>
        if sign*y <= 0:
            weight_vector = weight_vector + np.dot(x,y)
            w += 1
            c[w] = {
                "count" : 1,
                "weights" : weight_vector
            }
        # it's correct
        else:
            c[w]["count"] += 1
            continue
            
    passes[pass_key] = copy(c)


# In[13]:

for pass_key in [1,2,3,4]:
    c = passes[pass_key]
    
    # measure the training error on the voted perceptron
    correct_count = 0

    for x,y in train_12:
        # calculate the sign it generates through the voted perceptron
        sign = 0
        # iterate through each weight vector change
        for key in c:
            # how many training instances the weight vector persisted without changed
            c_i = c[key]["count"]
            # the weight vector before it changed after failing a training instance
            w_i = c[key]["weights"]

            pre_sign = 1 if np.dot(x, w_i.T) > 0 else -1

            sign += pre_sign*c_i

        if sign*y <= 0:
            continue
        else:
            correct_count += 1

    train_error = (len(train_12) - correct_count)/float(len(train_12))
    print("Training error on the voted perceptron is %f after %i passes through the training data" % (train_error, pass_key))

# create a new line between the print statements
print "\n"
    
for pass_key in [1,2,3,4]:
    c = passes[pass_key]
    
    # measure the testing error on the voted perceptron
    correct_count = 0

    for x,y in test_12:
        # calculate the sign it generates through the voted perceptron
        sign = 0
        # iterate through each weight vector change
        for key in c:
            # how many training instances the weight vector persisted without changed
            c_i = c[key]["count"]
            # the weight vector before it changed after failing a training instance
            w_i = c[key]["weights"]

            pre_sign = 1 if np.dot(x, w_i.T) > 0 else -1

            sign += pre_sign*c_i

        if sign*y <= 0:
            continue
        else:
            correct_count += 1

    test_error = (len(test_12) - correct_count)/float(len(test_12))
    print("Testing error on the voted perceptron is %f after %i passes through the training data" % (test_error, pass_key))


# In[25]:

# there isn't really much difference in the voted perceptron from the average perceptron except in 
# how the weight vector is added up when testing the model

for pass_key in [1,2,3,4]:
    c = passes[pass_key]
    
    # measure the training error on the average perceptron
    correct_count = 0

    for x,y in train_12:
        # calculate the sign it generates through the voted perceptron
        weight_vector = [0 for i in xrange(819)]
        for key in c:
            weight_vector = np.add(c[key]["weights"]*c[key]["count"], weight_vector)

        sign = np.dot(x, weight_vector.T)

        if sign*y <= 0:
            continue
        else:
            correct_count += 1

    train_error = (len(train_12) - correct_count)/float(len(train_12))
    print("Training error on the average perceptron is %f after %i passes through the training data" % (train_error, pass_key))

# create a new line between the print statements
print "\n"

for pass_key in [1,2,3,4]:
    c = passes[pass_key]
    
    # measure the training error on the average perceptron
    correct_count = 0

    for x,y in test_12:
        # calculate the sign it generates through the voted perceptron
        weight_vector = [0 for i in xrange(819)]
        for key in c:
            weight_vector = np.add(c[key]["weights"]*c[key]["count"], weight_vector)

        sign = np.dot(x, weight_vector.T)

        if sign*y <= 0:
            continue
        else:
            correct_count += 1

    test_error = (len(test_12) - correct_count)/float(len(test_12))
    print("Testing error on the average perceptron is %f after %i passes through the training data" % (test_error, pass_key))


# In[26]:

# Get the three highest words that rate positively and three lowest words that rate negatively

# load the dictionary
dictionary = []
with open('hw4dictionary.txt') as dictionary_file:
    for l in dictionary_file:
        
        dictionary.append(l.rstrip('\n').strip())

# Load the weight vector after the third pass
c = passes[3]
# Get the average weight vector
weight_vector = [0 for i in xrange(819)]
for key in c:
    weight_vector = np.add(c[key]["weights"]*c[key]["count"], weight_vector)

# get the 3 highest positions in the weight vector
highest_3 = sorted(enumerate(weight_vector), key=operator.itemgetter(1), reverse=True)[:3]

# get the 3 lowest positions in the weight vector
lowest_3 = sorted(enumerate(weight_vector), key=operator.itemgetter(1))[:3]

print "The three words with the highest positive weights are: "
for meh in highest_3:
    print dictionary[meh[0]]
print "The three words with the lowest negative weights are: "
for meh in lowest_3:
    print dictionary[meh[0]]
    


# In[18]:

# create the 1 vs all classifier for all of the labels
classifier = {1.0: {}, 2.0: {}, 3.0: {}, 4.0: {}, 5.0: {}, 6: {}}

# we need to prepare the data and filter it to such label 1 vs not label 1
for label in [1.0,2.0,3.0,4.0,5.0,6.0]:
    train_1vAll = [(x, 1) if y == label else (x, -1) for x,y in zip(X_train, y_train)]
    
    classifier[label]["train"] = train_1vAll
    
# build the classifier for each label by getting the weight vector after one pass
# on the training data
for label in [1.0,2.0,3.0,4.0,5.0,6.0]:
    train_1vAll = classifier[label]["train"]
    weights = perceptron(train_1vAll)
    
    classifier[label]["weights"] = weights

# create a confusion matrix
confusion = [[0 for i in xrange(6)] for i in xrange(7)]


# In[30]:

for x,y in zip(X_test, y_test):
    predictions = []
    
    # make a prediction against every 1vAll classifier
    for label in [1.0,2.0,3.0,4.0,5.0,6.0]:
        w = classifier[label]["weights"]
        sign = 1 if np.dot(w,x) > 0 else 0
        
        predictions.append(sign)
    
    # get the leading prediction from the classifier
    
    # if the classifer predicted multiple labels or no labels
    if predictions.count(1) > 1 or predictions.count(0) == 6:
        confusion[6][int(y-1)] += 1
        continue
    # if the classifier predict one label overall
    else:
        pred = predictions.index(1)
        confusion[pred][int(y-1)] += 1

label_count = {1.0: 0, 2.0: 0, 3.0: 0, 4.0: 0, 5.0: 0, 6.0: 0}
for y in y_test:
    label_count[y] += 1
    
# divide each of the each the entries in the confusion matrix by
# how many of the labels are avaiable in the test dataset
for i in xrange(len(confusion)):
    for j in xrange(6):
        confusion[i][j] = confusion[i][j]/float(label_count[float(j+1)])


# In[50]:

confusion_acc_ij = []
for i in xrange(6):
    confusion_acc_ij.append(confusion[i][i])
    
highest = sorted(enumerate(confusion_acc_ij), key=operator.itemgetter(1), reverse=True)[0]
lowest = sorted(enumerate(confusion_acc_ij), key=operator.itemgetter(1), reverse=True)[-1]

print("Perceptron with the classifier for class %i with highest accuracy %f" % (highest[0]+1, highest[1]))
print("Perceptron with the classifier for class %i with lowest accuracy %f" % (lowest[0]+1, highest[1]))

confusion_acc_ij = []
for i in xrange(6):
    for j in xrange(6):
        if i == j:
            continue
        else:
            confusion_acc_ij.append((i,j,confusion[i][j]))

mislabel = sorted(confusion_acc_ij, key=operator.itemgetter(2), reverse=True)[0]
print("Perceptron classifier most often mistakenly classifies an example in class %i as belonging to class %i" % (mislabel[1]+1, mislabel[0]+1))


# In[ ]:




# In[ ]:



